from setuptools import setup, find_packages

setup(
    name='ros_workshop',
    version='0.1.0',
    install_requires=[
        'tensorflow',
        'keras',
        'imageio',
        'matplotlib',
        'scikit-image',
        'numpy',
        ]
)
